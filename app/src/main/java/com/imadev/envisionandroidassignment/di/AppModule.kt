package com.imadev.envisionandroidassignment.di


import android.content.Context
import androidx.room.Room
import com.imadev.envisionandroidassignment.data.local.LibraryDao
import com.imadev.envisionandroidassignment.data.local.LibraryDatabase
import com.imadev.envisionandroidassignment.data.remote.OCRApi
import com.imadev.envisionandroidassignment.repositories.OCRRepositoryImp
import com.imadev.envisionandroidassignment.utils.Constants.BASE_URL
import com.imadev.envisionandroidassignment.utils.Constants.DATABASE_NAME
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {



    @Singleton
    @Provides
    fun provideLibraryDatabase(
        @ApplicationContext context: Context
    ) = Room.databaseBuilder(context, LibraryDatabase::class.java, DATABASE_NAME).build()

    @Singleton
    @Provides
    fun provideLibraryDao(db: LibraryDatabase) = db.libraryDatabase()

//    @Singleton
//    @Provides
//    fun provideOCRRepository(api:OCRApi,dao: LibraryDao) = OCRRepositoryImp(api,dao)


    @Singleton
    @Provides
    fun provideNoteApi(): OCRApi {

        val logging = HttpLoggingInterceptor()

        logging.level = HttpLoggingInterceptor.Level.BODY

        val client = OkHttpClient.Builder()
            .addInterceptor(logging)
            .retryOnConnectionFailure(true)
            .connectTimeout(50, TimeUnit.SECONDS)
            .readTimeout(50, TimeUnit.SECONDS)
            .writeTimeout(50, TimeUnit.SECONDS)
            .build()
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
            .create(OCRApi::class.java)
    }


}
