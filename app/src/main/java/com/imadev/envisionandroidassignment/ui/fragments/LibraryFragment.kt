package com.imadev.envisionandroidassignment.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.imadev.envisionandroidassignment.R
import com.imadev.envisionandroidassignment.adapters.LibraryListAdapter
import com.imadev.envisionandroidassignment.data.local.entities.LibraryEntity
import com.imadev.envisionandroidassignment.databinding.FragmentLibraryBinding
import com.imadev.envisionandroidassignment.ui.SharedViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

private const val TAG = "LibraryFragment"

class LibraryFragment : Fragment() {

    private var _binding: FragmentLibraryBinding? = null
    private val binding get() = _binding!!

    private val viewModel: SharedViewModel by activityViewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment

        _binding = FragmentLibraryBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        //Set-up recycler-view
        val adapter = LibraryListAdapter()
        binding.libraryList.adapter = adapter
        val mDividerItemDecoration = DividerItemDecoration(
            binding.libraryList.context,
            LinearLayoutManager.VERTICAL
        )
        binding.libraryList.addItemDecoration(mDividerItemDecoration)


        adapter.setOnClickListener { onClick(it) }


        //Collect list of library
        lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.library.collect {
                    adapter.submitList(it)
                }
            }
        }


    }

    private fun onClick(item: LibraryEntity) {
        Log.d(TAG, "onClick: ${item.text}")

    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

}