package com.imadev.envisionandroidassignment.ui

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.imadev.envisionandroidassignment.data.local.entities.LibraryEntity
import com.imadev.envisionandroidassignment.data.remote.response.String
import com.imadev.envisionandroidassignment.repositories.OCRRepositoryImp
import com.imadev.envisionandroidassignment.utils.Event
import com.imadev.envisionandroidassignment.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File
import javax.inject.Inject


private const val TAG = "CaptureFragment"


@HiltViewModel
class SharedViewModel @Inject constructor(
    val repository: OCRRepositoryImp
) : ViewModel() {

    private val _isPermissionGranted = MutableLiveData(Event(false))
    val isPermissionGranted: LiveData<Event<Boolean>> get() = _isPermissionGranted


    private val _textResponseMutableLiveData = MutableLiveData<Event<Resource<String>>>()
    val textResponseLiveData: LiveData<Event<Resource<String>>> get() = _textResponseMutableLiveData

    private val _isInserted = MutableLiveData(Event(false))
    val isInserted: LiveData<Event<Boolean>> get() = _isInserted


    fun setPermissionGranted(isGranted: Boolean) {
        Log.d(TAG, "setPermissionGranted: $isGranted")
        _isPermissionGranted.postValue(Event(isGranted))
    }

    fun uploadPhoto(imageFile: File) = viewModelScope.launch {

        val requestBody = imageFile.asRequestBody("image/*".toMediaTypeOrNull())
        val part = MultipartBody.Part.createFormData("photo", imageFile.name, requestBody)
        _textResponseMutableLiveData.postValue(Event(Resource.Loading(null)))

        val result = repository.uploadPhoto(part)
        _textResponseMutableLiveData.postValue(Event(result))

    }


    fun insertParagraph(data: LibraryEntity) = viewModelScope.launch {
        //if the paragraph inserted successfully will return a number grater than zero
        _isInserted.postValue(Event(repository.insertParagraph(data) >= 1L))
    }

    //receive flow of library
    val library = repository.getAllLibrary()

}

