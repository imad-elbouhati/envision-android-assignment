package com.imadev.envisionandroidassignment.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.tabs.TabLayoutMediator
import com.imadev.envisionandroidassignment.R
import com.imadev.envisionandroidassignment.adapters.ViewPagerAdapter
import com.imadev.envisionandroidassignment.databinding.FragmentViewPagerBinding
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class ViewPagerFragment : Fragment() {

    private var _binding: FragmentViewPagerBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentViewPagerBinding.inflate(inflater, container, false)
        val pagerAdapter = ViewPagerAdapter(this)
        binding.pager.adapter = pagerAdapter


        TabLayoutMediator(binding.tabLayout, binding.pager) { tab, position ->
            when (position) {
                0 -> tab.text = getString(R.string.capture)
                1 -> tab.text = getString(R.string.library)
            }
        }.attach()

        binding.pager.setCurrentItem(1,false)


        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        onBackPress()

    }

    private fun onBackPress() {
        activity?.onBackPressedDispatcher?.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {

                    if (binding.pager.currentItem == 0) {
                        activity?.finish()
                    } else {
                        binding.pager.currentItem = binding.pager.currentItem - 1
                    }
                }
            })
    }


    fun goToLibrary() {
        binding.tabLayout.selectTab(binding.tabLayout.getTabAt(1))
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}