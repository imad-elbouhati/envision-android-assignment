package com.imadev.envisionandroidassignment.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityNodeInfo
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import com.google.common.util.concurrent.ListenableFuture
import com.imadev.envisionandroidassignment.R
import com.imadev.envisionandroidassignment.data.local.entities.LibraryEntity
import com.imadev.envisionandroidassignment.data.remote.response.String
import com.imadev.envisionandroidassignment.databinding.FragmentCaptureBinding
import com.imadev.envisionandroidassignment.ui.SharedViewModel
import com.imadev.envisionandroidassignment.utils.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import java.io.File
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors


private const val TAG = "CaptureFragment"

private const val SNACKBAR_LENGTH = 15*1000
@AndroidEntryPoint
class CaptureFragment : Fragment() {

    private var _binding: FragmentCaptureBinding? = null
    private val binding get() = _binding!!

    private val viewModel: SharedViewModel by activityViewModels()

    private lateinit var cameraProviderFuture: ListenableFuture<ProcessCameraProvider>

    private lateinit var imageCapture: ImageCapture

    private lateinit var cameraExecutor: ExecutorService

    private var mParagraph: kotlin.String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentCaptureBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.binding.previewView.implementationMode = PreviewView.ImplementationMode.COMPATIBLE

        cameraExecutor = Executors.newSingleThreadExecutor()

        viewModel.isPermissionGranted.observe(viewLifecycleOwner, { event ->

            event?.getContentIfNotHandled()?.let { isGranted ->
                Log.d(TAG, "onViewCreated: ")
                if (isGranted) {
                    binding.previewView.post {
                        startCamera()
                    }
                }
            }
        })


        binding.captureButton.setOnClickListener {
            takePicture()
        }


        binding.saveTextButton.setOnClickListener {

            mParagraph?.let {

                viewModel.insertParagraph(
                    LibraryEntity(
                        text = it,
                        timestamp = System.currentTimeMillis()
                    )
                )
            }

        }

        subscribeToObservers()

    }

    private fun subscribeToObservers() {

        viewModel.textResponseLiveData.observe(viewLifecycleOwner, { event ->
            event?.peekContent()?.let {
                val data = it.data
                when (it) {
                    is Resource.Loading -> {
                        binding.captureButton.hide()
                        binding.loadingButton.apply {
                            show()
                            sendAccessibilityEvent(AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED)
                        }

                    }

                    is Resource.Success -> {
                        binding.captureButton.isEnabled = true
                        binding.saveTextButton.isEnabled = true
                        Log.d(TAG, "observeResult Success: $data")
                        binding.updateUI(data)
                    }
                    is Resource.Error -> {
                        binding.loadingButton.show()
                    }
                }
            }
        })






        viewModel.isInserted.observe(viewLifecycleOwner, { event ->
            event.peekContent().let {
                if (it) {
                    binding.restPreviewUI()
                    binding.saveTextButton.isEnabled = false

                    val snackbar = requireView().showSnackbar(
                        requireView(),
                        getString(R.string.text_saved_to_library),
                        SNACKBAR_LENGTH,
                        getString(R.string.go_to_library)
                    ) {
                        (parentFragment as ViewPagerFragment).goToLibrary()
                    }
                    //Request focus for snackbar
                    snackbar.view.focusAccessibility()

                }

            }
        })

    }

    private fun View.focusAccessibility() = lifecycleScope.launchWhenResumed {
        delay(300)
        this@focusAccessibility.performAccessibilityAction(AccessibilityNodeInfo.ACTION_ACCESSIBILITY_FOCUS, null)
    }


    private fun FragmentCaptureBinding.restPreviewUI() {
        captureButton.show()
        previewView.show()
        saveTextButton.hide()
        scrollView.hide()
    }


    private fun FragmentCaptureBinding.updateUI(data: String?) {
        captureButton.hide()
        loadingButton.hide()
        previewView.hide()
        saveTextButton.show()
        mParagraph = data?.response?.paragraphs?.joinToString()
        //Set paragraph text
        paragraph.text = mParagraph
        scrollView.show()
        paragraph.sendAccessibilityEvent(AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED)

    }


    private fun takePicture() {

        val imageFilenamePrefix = System.currentTimeMillis().toDate()

        //Create Image file
        val imageFile = File(requireContext().cacheDir, "$imageFilenamePrefix.png")
        val outputFileOptions = ImageCapture.OutputFileOptions.Builder(imageFile).build()


        imageCapture.takePicture(outputFileOptions, cameraExecutor,
            object : ImageCapture.OnImageSavedCallback {
                override fun onImageSaved(outputFileResults: ImageCapture.OutputFileResults) {
                    viewModel.uploadPhoto(imageFile)
                }

                override fun onError(exception: ImageCaptureException) {
                }
            }
        )

    }


    private fun startCamera() {
        cameraProviderFuture = ProcessCameraProvider.getInstance(requireContext())

        cameraProviderFuture.addListener({
            val cameraProvider = cameraProviderFuture.get()
            bindPreview(cameraProvider)
        }, ContextCompat.getMainExecutor(requireContext()))
    }

    private fun bindPreview(cameraProvider: ProcessCameraProvider) {
        val cameraSelector: CameraSelector = CameraSelector.Builder()
            .requireLensFacing(CameraSelector.LENS_FACING_BACK)
            .build()

        val preview: Preview = Preview.Builder()
            .build()
            .also {
                it.setSurfaceProvider(binding.previewView.surfaceProvider)
            }

        imageCapture = ImageCapture.Builder()
            .setTargetRotation(binding.previewView.display.rotation)
            .build()


        // Unbind use cases before rebinding
        cameraProvider.unbindAll()

        // Unbind use cases before rebinding
        cameraProvider.bindToLifecycle(
            viewLifecycleOwner,
            cameraSelector,
            preview,
            imageCapture
        )

    }


    override fun onDestroyView() {
        _binding = null
        cameraExecutor.shutdown()
        super.onDestroyView()
    }
}