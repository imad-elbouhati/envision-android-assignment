package com.imadev.envisionandroidassignment.ui

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.imadev.envisionandroidassignment.R
import com.imadev.envisionandroidassignment.databinding.ActivityMainBinding
import com.imadev.envisionandroidassignment.utils.showSnackbar
import dagger.hilt.android.AndroidEntryPoint


private const val TAG = "MainActivity"

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {


    private lateinit var layout: View
    private lateinit var binding: ActivityMainBinding

    private val viewModel: SharedViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        layout = binding.mainLayout
        setContentView(view)

        requestPermission(view)
    }

    private val requestPermissionLauncher = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isGranted ->
        if (isGranted) {
            viewModel.setPermissionGranted(isGranted)
        } else {
            viewModel.setPermissionGranted(isGranted)
            showAlertDialog()
        }
    }


    private fun requestPermission(view: View) {
        when {
            ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_GRANTED -> {
                Log.d(TAG, "requestPermission: fefefe")
                viewModel.setPermissionGranted(true)
            }

            ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                Manifest.permission.CAMERA
            ) -> {
                layout.showSnackbar(
                    view,
                    getString(R.string.permission_required),
                    Snackbar.LENGTH_INDEFINITE,
                    getString(R.string.ok)
                ) {
                    launchPermissionRequest()
                }
            }

            else -> {
                launchPermissionRequest()
            }
        }
    }

    private fun launchPermissionRequest() {
        requestPermissionLauncher.launch(
            Manifest.permission.CAMERA
        )
    }


    private fun showAlertDialog() {
        val alertDialogBuilder = AlertDialog.Builder(this)


        alertDialogBuilder.apply {
            setMessage(
                getString(R.string.alert_dialog_message)
            )

            setPositiveButton(
                getString(R.string.yes)
            ) { _, _ ->
                launchPermissionRequest()
            }

            setNegativeButton(
                getString(R.string.no)
            ) { _, _ ->
                finish()
            }
        }


        val alertDialog = alertDialogBuilder.create()
        alertDialog.show()
    }

}