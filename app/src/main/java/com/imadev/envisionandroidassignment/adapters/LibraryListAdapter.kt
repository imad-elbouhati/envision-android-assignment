package com.imadev.envisionandroidassignment.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.imadev.envisionandroidassignment.data.local.entities.LibraryEntity
import com.imadev.envisionandroidassignment.databinding.LibraryRowItemBinding
import com.imadev.envisionandroidassignment.utils.toDate


class LibraryListAdapter :
    ListAdapter<LibraryEntity, LibraryListAdapter.ViewHolder>(LibraryDiffCallback()) {


    private var listener: ((LibraryEntity) -> Unit)? = null

    fun setOnClickListener(listener: ((LibraryEntity) -> Unit)) {
        this.listener = listener
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.binding.root.setOnClickListener {
            listener?.invoke(item)
        }
        holder.bind(item)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(val binding: LibraryRowItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: LibraryEntity) {
            binding.title.text = item.timestamp.toDate()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = LibraryRowItemBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }
}


class LibraryDiffCallback : DiffUtil.ItemCallback<LibraryEntity>() {

    override fun areItemsTheSame(oldItem: LibraryEntity, newItem: LibraryEntity): Boolean {
        return oldItem.id == newItem.id
    }


    override fun areContentsTheSame(oldItem: LibraryEntity, newItem: LibraryEntity): Boolean {
        return oldItem == newItem
    }


}