package com.imadev.envisionandroidassignment.adapters

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.imadev.envisionandroidassignment.ui.fragments.CaptureFragment
import com.imadev.envisionandroidassignment.ui.fragments.LibraryFragment

private const val NUM_PAGES = 2

class ViewPagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int = NUM_PAGES

    override fun createFragment(position: Int): Fragment {

        return when(position) {
            0 -> CaptureFragment()
            1 -> LibraryFragment()
            else -> Fragment()
        }
    }

   
}