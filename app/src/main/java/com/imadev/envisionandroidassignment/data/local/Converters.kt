package com.imadev.envisionandroidassignment.data.local

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.imadev.envisionandroidassignment.data.local.entities.LibraryEntity

class Converters {
    @TypeConverter
    fun fromList(list: List<LibraryEntity>): String {
        return Gson().toJson(list)
    }

    @TypeConverter
    fun toList(string: String): List<LibraryEntity> {
        return Gson().fromJson(string, object : TypeToken<List<LibraryEntity>>() {}.type)
    }
}