package com.imadev.envisionandroidassignment.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.imadev.envisionandroidassignment.data.local.entities.LibraryEntity
import kotlinx.coroutines.flow.Flow


@Dao
interface LibraryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertParagraph(data: LibraryEntity): Long


    @Query("SELECT * FROM libraries ORDER BY timestamp DESC")
    fun getLibrary(): Flow<List<LibraryEntity>>
}