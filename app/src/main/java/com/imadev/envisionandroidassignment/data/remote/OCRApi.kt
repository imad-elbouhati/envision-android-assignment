package com.imadev.envisionandroidassignment.data.remote

import com.imadev.envisionandroidassignment.data.remote.response.String
import okhttp3.MultipartBody
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part


interface OCRApi {

    @Multipart
    @POST("readDocument")
    suspend fun postPhoto(@Part photo: MultipartBody.Part): String
}