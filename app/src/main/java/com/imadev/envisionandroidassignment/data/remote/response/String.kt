package com.imadev.envisionandroidassignment.data.remote.response

import kotlin.String

data class String(
    val response: Response
)

data class Response(
    val locale: String,
    val paragraphs: List<String>
)