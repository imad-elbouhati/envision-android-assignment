package com.imadev.envisionandroidassignment.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.imadev.envisionandroidassignment.data.local.entities.LibraryEntity


@Database(
    entities = [LibraryEntity::class],
    version = 1
)
@TypeConverters(Converters::class)
abstract class LibraryDatabase : RoomDatabase() {

    abstract fun libraryDatabase(): LibraryDao
}