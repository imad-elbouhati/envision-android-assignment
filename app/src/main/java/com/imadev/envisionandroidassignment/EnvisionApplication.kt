package com.imadev.envisionandroidassignment

import android.app.Application
import dagger.hilt.android.HiltAndroidApp


@HiltAndroidApp
class EnvisionApplication:Application() {
}