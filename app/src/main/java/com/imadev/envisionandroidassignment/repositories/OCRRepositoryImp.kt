package com.imadev.envisionandroidassignment.repositories

import com.imadev.envisionandroidassignment.data.local.LibraryDao
import com.imadev.envisionandroidassignment.data.local.entities.LibraryEntity
import com.imadev.envisionandroidassignment.data.remote.OCRApi
import com.imadev.envisionandroidassignment.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import okhttp3.MultipartBody
import javax.inject.Inject

class OCRRepositoryImp @Inject constructor(
    private val api: OCRApi,
    private val dao: LibraryDao
) : OCRRepository {

    override suspend fun uploadPhoto(body: MultipartBody.Part) = safeApiCall {
        api.postPhoto(body)
    }

    override suspend fun insertParagraph(data: LibraryEntity) = dao.insertParagraph(data)


    override fun getAllLibrary(): Flow<List<LibraryEntity>> {
        return dao.getLibrary()
    }
}


suspend fun <T> safeApiCall(apiCall: suspend () -> T): Resource<T> {

    return withContext(Dispatchers.IO) {
        try {
            Resource.Success(apiCall.invoke())
        } catch (throwable: Throwable) {
            Resource.Error(throwable, null)

        }
    }
}

