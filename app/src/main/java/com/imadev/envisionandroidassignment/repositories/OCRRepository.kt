package com.imadev.envisionandroidassignment.repositories

import com.imadev.envisionandroidassignment.data.local.entities.LibraryEntity
import com.imadev.envisionandroidassignment.data.remote.response.String
import com.imadev.envisionandroidassignment.utils.Resource
import kotlinx.coroutines.flow.Flow
import okhttp3.MultipartBody

interface OCRRepository {

    suspend fun uploadPhoto(body: MultipartBody.Part): Resource<String>

    suspend fun insertParagraph(data: LibraryEntity): Long

    fun getAllLibrary(): Flow<List<LibraryEntity>>
}