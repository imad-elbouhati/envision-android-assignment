package com.imadev.envisionandroidassignment.utils

import android.graphics.Color
import android.view.View
import com.google.android.material.snackbar.Snackbar


fun View.hide() {
    this.visibility = View.GONE
}

fun View.show() {
    this.visibility = View.VISIBLE
}


fun View.showSnackbar(
    view: View,
    msg: String,
    length: Int,
    actionMessage: CharSequence?,
    action: (View) -> Unit
): Snackbar {
    val snackbar = Snackbar.make(view, msg, length)
    snackbar.apply {
        setBackgroundTint(Color.WHITE)
        setTextColor(Color.BLACK)
        setActionTextColor(Color.parseColor("#6200EE"))
    }

    if (actionMessage != null) {
        snackbar.setAction(actionMessage) {
            action(this)
        }.show()
    } else {
        snackbar.show()
    }
    return snackbar
}


