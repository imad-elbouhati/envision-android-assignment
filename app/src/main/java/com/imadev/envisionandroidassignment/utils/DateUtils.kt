package com.imadev.envisionandroidassignment.utils

import java.text.SimpleDateFormat
import java.util.*


fun Long.toDate(pattern: String = "yyyy-MM-dd HH:mm"): String {
    return SimpleDateFormat(
        pattern,
        Locale.getDefault()
    ).format(this)
}