package com.imadev.envisionandroidassignment.utils

object Constants {

    const val BASE_URL = "https://eg-beta.firebaseapp.com/api/v1/"
    const val DATABASE_NAME="library_db"
}